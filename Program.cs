﻿using System;
using Entities.Classes;
using Entities.Exceptions;

class Program
{
   static void Main()
   {
      try
      {
         var acc = new Account();
         Console.WriteLine("Enter account data");

         Console.Write("Number: ");
         acc.number = int.Parse(Console.ReadLine());

         Console.Write("Holder: ");
         acc.holder = Console.ReadLine();

         Console.Write("Initial balance: ");
         acc.balance = int.Parse(Console.ReadLine());

         Console.Write("Withdraw limit: ");
         acc.withdrawLimit = int.Parse(Console.ReadLine());

         Console.WriteLine();

         Console.Write("Enter amount for withdraw: ");
         int withdraw = int.Parse(Console.ReadLine());

         if (withdraw > acc.withdrawLimit) {
            throw new ExceedException("Withdraw error: The amount exceeds the withdraw limit");
         }         
         
         else if (acc.balance < withdraw) {
            throw new NotEnoughException("Withdraw error: Not enough balance");
         }

         Console.WriteLine($"New balance: {acc.balance - withdraw}");
      }

      catch (ExceedException e)
      {
         Console.WriteLine(e.Message);
      }

      catch (NotEnoughException e)
      {
         Console.WriteLine(e.Message);
      }
   }
}