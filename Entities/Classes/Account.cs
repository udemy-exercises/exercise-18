namespace Entities.Classes
{
   class Account
   {
      public int number;
      public string holder;
      public double balance;
      public double withdrawLimit;
   }
}