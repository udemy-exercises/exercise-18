using System;

namespace Entities.Exceptions
{
   class ExceedException : ApplicationException
   {
      public ExceedException (string message) : base (message)
      {
      }
   }
}