using System;

namespace Entities.Exceptions
{
   class NotEnoughException : ApplicationException
   {
      public NotEnoughException(string message) : base (message)
      {
      }
   }
}